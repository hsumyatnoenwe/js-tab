$(document).ready(function(){
	function nth_parent(tag,nth) {
	  if (!tag.jquery) {
	    tag = $(tag);
	  }
	 var parent_tag	 = tag;

	 for (var i = 0; i < nth; i++) {
	    parent_tag = $(parent_tag).parent();
	  };
	 return parent_tag;
	}
	if ($('.js-tab-content').length) {
	  $('.js-tab-content .tabs a').click(function(e) {
	    e.preventDefault();
	    var parent = nth_parent($(this), 4);
	    var tagName = $(this).attr('data');

	   parent.find('.tabs a').removeClass('active');
	    $(this).addClass('active');

	   	parent.find('.tb-contents').removeClass('active');
	    parent.find('.tb-contents'+tagName).addClass('active');
	  });
	}
});